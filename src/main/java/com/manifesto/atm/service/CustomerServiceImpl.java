package com.manifesto.atm.service;

import com.manifesto.atm.model.Customer;
import org.apache.log4j.Logger;

import static com.manifesto.atm.utils.Constants.ERROR_CUSTOMER_OUT_OF_FUNDS;

public class CustomerServiceImpl implements CustomerService {

    private static Logger logger = Logger.getLogger(CustomerServiceImpl.class);

    /**
     * Authenticates the customer using their pin code.
     *
     * @param customer
     * @param pin
     * @return
     */
    public Boolean authenticateCustomer(Customer customer, int pin) {
        return customer.getPin() == pin; //Normally this call would be to an external service or database.
    }

    /**
     * Returns the customers current balance.
     *
     * @param customer
     * @return
     */
    public int getBalance(Customer customer) {
        return customer.getBalance(); //Normally this call would be to an external service or database.
    }

    /**
     * Returns the customers current over draft.
     *
     * @param customer
     * @return
     */
    public int getOverDraft(Customer customer) {
        return customer.getOverdraft(); //Normally this call would be to an external service or database.
    }

    /**
     * Returns the customers account number.
     *
     * @param customer
     * @return
     */
    public int getAccountNumber(Customer customer) {
        return customer.getAccountNumber(); //Normally this call would be to an external service or database.
    }

    /**
     * Withdraws funds from the customers current balance.
     *
     * @param customer
     * @param amount
     * @return
     */
    public String withDrawFunds(Customer customer, int amount) {
        int balance = customer.getBalance() - amount; //Normally this call would be to an external service or database.
        if(balance < 0) {
            logger.error("Customer out of funds: " + ERROR_CUSTOMER_OUT_OF_FUNDS);
            return ERROR_CUSTOMER_OUT_OF_FUNDS;
        } else {
            return Integer.toString(balance);
        }
    }

}
