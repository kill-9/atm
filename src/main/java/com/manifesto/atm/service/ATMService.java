package com.manifesto.atm.service;

import com.manifesto.atm.model.ATMMachine;

public interface ATMService {

    int getATMTotalBalance(ATMMachine atmMachine);
    String withDrawFunds(ATMMachine atmMachine, int amount);

}
